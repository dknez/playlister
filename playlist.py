# MRDANJE PLAYLISTE
# https://www.youtube.com/watch?v=yM-XC_xPAPU&list=PL5wgbebfWKBvSND7V8pqciYx3jcZEtpvg
# GET https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=PL5wgbebfWKBvSND7V8pqciYx3jcZEtpvg&key=AIzaSyB-MEtZh91MTx8L7MY5jD-YrCx1FQtsbcA

YOUTUBE_API_REQUEST = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&key="
API_KEY = ""
PLAYLIST_ID = "&playlistId="
YOUTUBE_LINK = "https://www.youtube.com/watch?v="

ARTIST_SONG_REGEX = "(.+)\s*-\s*(.+)"
YT_PLAYLIST_ID_REGEX = "^.*(youtu.be\/|list=)([^#\&\?]*).*"
OUTPUT_FILE = "playlist.xspf"
ENCODING = "utf-8"

import sys
import argparse
import json
import re
import xml.etree.cElementTree as ET
import urllib.error
from urllib.request import urlopen

# http://stackoverflow.com/questions/8924173/how-do-i-print-bold-text-in-python
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

# Mehtod used for pretty print of the XML file
# copied from http://effbot.org/zone/element-lib.htm
def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def buildRequestString(playlistId):
    request = YOUTUBE_API_REQUEST + API_KEY + PLAYLIST_ID + playlistId
    return request

def generateXspfFile(json_data):
    result = False

    pattern = re.compile(ARTIST_SONG_REGEX, re.MULTILINE)
    
    playlist = ET.Element("playlist")
    playlist.set("version", "1")
    playlist.set("xmlns", "http://xspf.org/ns/0/")
    
    trackList = ET.SubElement(playlist, "trackList")    
      
    for item in json_data["items"]:
        videoId = item["snippet"]["resourceId"]["videoId"]
        videoName = item["snippet"]["title"]
        
        try:
            matcher = pattern.match(videoName)
            artistName = matcher.group(1)
            songName = matcher.group(2)
        except AttributeError:
            print("Oops, this song's name cannot be parsed: " + videoName)
            continue
        
        track = ET.SubElement(trackList, "track")
        
        location = ET.SubElement(track, "location").text = YOUTUBE_LINK + videoId
        creator = ET.SubElement(track, "creator").text = artistName
        title = ET.SubElement(track, "title").text = songName
              
    tree = ET.ElementTree(playlist)
    indent(playlist)
    
    try: 
        tree.write(OUTPUT_FILE, ENCODING, xml_declaration=True)
        result = True
    except ET.ParseError:
        print("Oops! Cannot write .xspf file.")
    
    return result
    
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Create new playlist file from YouTube playlist URL.')
    parser.add_argument("ytlink", help="YouTube playlist URL")
    args = parser.parse_args()
    ytLink = args.ytlink
    
    try: 
        pattern = re.compile(YT_PLAYLIST_ID_REGEX, re.MULTILINE)
        matcher = pattern.match(ytLink)
        playlistId = matcher.group(2)
    except AttributeError:
        print("Oops, cannot parse playlist ID from the URL you provided!")
        print("Please check your URL! It seems like no &list parameter is named in your URL!")
        print("*** You should put the URL in quotes! ***")
        sys.exit(1)
    
    request = buildRequestString(playlistId)
    
    try:
        response = urlopen(request).read().decode('utf8')
        json_data = json.loads(response)
    except urllib.error.URLError as e:
        print("Oops, cannot fetch JSON code from YouTube server. Reason: " + e.reason)
        print("Please check your URL!")
        sys.exit(1)
    
    result = generateXspfFile(json_data)
    
    if (result is True):
        print("Playlist succesfully generated!")
    else:
        print(color.RED + "Cannot write playlist file to disk. :(" + color.END)
